
package mimetypes

import (
    "bufio"
    "fmt"
    "mime"
    "os"
    "strings"
    "sync"
    "bitbucket.org/hooblei/sets"
)

type MimeTypes struct {
    types map[string]sets.Set
    extensions map[string]sets.Set
    lock sync.RWMutex
}

// Read mime type definitions form given file path (e.g. /etc/mime.types)
func (mt *MimeTypes) LoadMimeFile(fpath string) {
    file, err := os.Open(fpath)
    if err != nil {
        panic(err)
    }

    defer file.Close()
    scanner := bufio.NewScanner(file)

    for scanner.Scan() {
        fields := strings.Fields(scanner.Text())
        if len(fields) <= 1 || fields[0][0] == '#' {
            continue
        }
        mtype := fields[0]
        for _, ext := range fields[1:] {
            if ext[0] == '#' {
                break
            }
            mt.AddTypeExtension(mtype, ext)
        }
    }

    if err := scanner.Err(); err != nil {
        panic(err)
    }
}

func (mt *MimeTypes) AddTypeExtension(mtype string, ext string) error {
    mt.lock.Lock()
    defer mt.lock.Unlock()
    e := strings.TrimLeft(ext, ".")
    t, _, err := mime.ParseMediaType(mtype)
    if err != nil {
        return err
    }

    if _, exists := mt.types[t]; !exists {
        mt.types[t] = sets.Set{}
    }
    sets.Add(mt.types[t], e)
    if _, exists := mt.extensions[e]; !exists {
        mt.extensions[e] = sets.Set{}
    }
    sets.Add(mt.extensions[e], t)

    return nil
}

func (mt *MimeTypes) HasType(mtype string) bool {
    var exists bool

    if t, _, err := mime.ParseMediaType(mtype); err == nil {
        _, exists = mt.types[t]
    }

    return exists
}

func (mt *MimeTypes) HasExtension(ext string) bool {
    _, exists := mt.extensions[ext]

    return exists
}

func (mt *MimeTypes) TypeExtensions(mtype string) []string {
    var exts []string
    var err error

    if mtype, _, err = mime.ParseMediaType(mtype); err == nil {
        mt.lock.Lock()
        if set, exists := mt.types[mtype]; exists {
            exts = sets.StringValues(set)
        }
        mt.lock.Unlock()
    }

    return exts
}

func (mt *MimeTypes) TypeExtension(mtype string) string {
    var ext string

    if exts := mt.TypeExtensions(mtype); len(exts) > 0 {
        ext = exts[0]
    }

    return ext
}

func (mt *MimeTypes) ExtensionTypes(ext string) []string {
    var mtypes []string

    mt.lock.Lock()
    if set, exists := mt.extensions[ext]; exists {
        mtypes = sets.StringValues(set)
    }
    mt.lock.Unlock()

    return mtypes
}

func (mt *MimeTypes) ExtensionType(ext string) string {
    var mtype string

    if mtypes := mt.ExtensionTypes(ext); len(mtypes) > 0 {
        mtype = mtypes[0]
    }

    return mtype
}

func NewMimeTypes(fpaths ...string) *MimeTypes {
    mt := &MimeTypes{
        types: map[string]sets.Set{},
        extensions: map[string]sets.Set{},
    }

    for _, p := range fpaths {
        mt.LoadMimeFile(p)
    }

    return mt
}

func main() {
    mt := NewMimeTypes("./media-mime.types")
    fmt.Println(mt.TypeExtension("image/png"))
    fmt.Println(mt.ExtensionType("svg"))
    fmt.Println(mt.HasExtension("svg"))
    fmt.Println(mt.HasExtension("foo"))
    fmt.Println(mt.HasType("image/png"))
    fmt.Println(mt.HasType("text/markdown;charset=utf-8"))
}

